package demo.springbatch.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import demo.springbatch.entity.SalesArea;

@Component("iWriter2")
public class IWriter2 implements ItemWriter<SalesArea> {

	public void write(List<? extends SalesArea> items) throws Exception {
		for (SalesArea salesArea : items) {
//			System.out.println("w2:" + salesArea);
		}
	}

}
