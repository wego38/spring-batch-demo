package demo.springbatch.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

@Component("testWriter")
public class TestWriter implements ItemWriter<Object> {

	public TestWriter() {
	}

	public void write(List<? extends Object> items) throws Exception {
		System.out.println();
	}

}
