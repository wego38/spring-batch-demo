package demo.springbatch.tasklet;

import java.util.Date;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

@Component
public class TestTasklet implements Tasklet {

	public TestTasklet() {
		// TODO Auto-generated constructor stub
	}

	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("tasklet1111");
		System.out.println(chunkContext.getAttribute("test"));
		chunkContext.setAttribute("test", new Date());
		return null;
	}

}
