package demo.springbatch.entity;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * @author tanshuai
 */
@Getter
@Setter
public abstract class ECBasicData {

	private Date syncTime;						// 37- 导入时间
	
	private String syncFile;					// 38-导入文件�?
	
	private String syncStatus;					// 39-导入状�??
	
	private String syncErr;
}
