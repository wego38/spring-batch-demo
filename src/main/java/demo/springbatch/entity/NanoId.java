package demo.springbatch.entity;

import java.io.Serializable;

/**
 * @author tanshuai
 */
public abstract class NanoId implements Serializable {

//    @Id
//    @NotBlank(groups = Edit.class, message = "唯一标识不能为空")
    private String id = getNanoId();

	public void setId(String id) {
		this.id = id;
	}

    public static String getNanoId() {
        return System.nanoTime() + "";
    }
}
