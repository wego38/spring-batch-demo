package demo.springbatch.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the T_EC_STORE database table.
 * 
 */
@Component
@Getter
@Setter
public class EcStore /*extends ECBasicData*/ implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String datePattern="yyyy/MM/dd HH:mm:ss";
	private String storeId;

	private String brand;

	private String city;

	private Date createdDate;

	private String memo1;

	private String memo2;

	private String nameCh;

	private String nameEn;

	private String openingTime;

	private Integer status;

	private String storeCode;

	private Integer type;

	public EcStore() {
	}
	
	public String getDatePattern() {
		return datePattern;
	}

}