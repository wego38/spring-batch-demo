package demo.springbatch.entity;


//@Entity
//@Table(name = "T_SALES_AREA")
public class SalesArea extends BasicData {

	//@Column(name = "AREA_NAME")
	private String areaName;
	
	//@Column(name = "AREA_CODE")
	private String areaCode;
	
	private String description;
	
	//@Column(name = "PARENT_ID")
	private String parentId;
	
	//@Column(name = "IMPORT_ID")
	private String importId;
	
	//@Column(name = "IS_DELETE")
	private String isDelete;

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getImportId() {
		return importId;
	}

	public void setImportId(String importId) {
		this.importId = importId;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "SalesArea [areaName=" + areaName + "]";
	}
	
}
