package demo.springbatch.entity;

import java.util.Date;

/**
 * @author tanshuai
 */
public abstract class BasicData extends NanoId {

//    @Column(updatable = false, name = "create_time")
//    @Temporal(TemporalType.TIMESTAMP)
    protected Date createTime = new Date();

   public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	// @Column
    protected String creater;

  //  @Column(updatable = true, name = "update_time")
  //  @Temporal(TemporalType.TIMESTAMP)
    protected Date updateTime = new Date();

 //   @Column
    protected String modifier;
}
