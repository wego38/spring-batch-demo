package demo.springbatch.job;

import java.util.Date;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class DemoJob {
	
	@Autowired  
    private JobLauncher jobLauncher;  
  
    @Autowired
    @Qualifier("testJob1")
    private Job demoJob;  
  
    @Autowired  
    JobParametersBuilder jobParameterBulider; 

	public void start() throws Exception {
		jobParameterBulider.addDate("date", new Date());  
		System.out.println("12333344");
        jobLauncher.run(demoJob, jobParameterBulider.toJobParameters());  
        
	}
	
}
