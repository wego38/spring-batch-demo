package demo.springbatch.reader;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import demo.springbatch.entity.EcStore;
import demo.springbatch.reader.fieldmapper.TestFieldMapper;

@Component("testReader")
public class TestReader implements ItemReader<EcStore> {
	static String[] aa=new String[]{};
	
	FlatFileItemReader<EcStore> itemReader = new FlatFileItemReader<EcStore>();
	
	@PostConstruct
	public void init(){
		itemReader.setResource(new FileSystemResource("D:\\gap\\sftp\\EC\\store.csv"));
		//DelimitedLineTokenizer defaults to comma as its delimiter
		DefaultLineMapper<EcStore> lineMapper = new DefaultLineMapper<EcStore>();
		DelimitedLineTokenizer linetoken=new DelimitedLineTokenizer();
		BeanWrapperFieldSetMapper<EcStore> mapper=new BeanWrapperFieldSetMapper<EcStore>();
//		mapper.setPrototypeBeanName("ecStore");
		mapper.setTargetType(EcStore.class);
		linetoken.setDelimiter(",");
		linetoken.setNames(new String[]{"storeId","brand","nameCh","nameEn","city","openingTime","type","status","storeCode","createdDate"});
		lineMapper.setLineTokenizer(linetoken);
		lineMapper.setFieldSetMapper(mapper);
		itemReader.setLineMapper(lineMapper);
	}
	public EcStore read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		itemReader.open(new ExecutionContext());
		EcStore player =  itemReader.read();
		return player;
	}

}
