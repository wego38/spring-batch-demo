package demo.springbatch.processor;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import demo.springbatch.entity.BasicData;
import demo.springbatch.entity.SalesArea;

@Component("iProcessor2")
public class IProcessor2 implements ItemProcessor<Object, Object> {

	public Object process(Object item) throws Exception {
		//System.out.println(item); 
        return item;
	}

}
