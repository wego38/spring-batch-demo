package demo.springbatch.processor;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.springbatch.entity.BasicData;
import demo.springbatch.entity.SalesArea;
import demo.springbatch.util.TestUtil;

@Component("iProcessor")
public class IProcessor implements ItemProcessor<Object, Object> {

	@Autowired
	private TestUtil util;
	
	private Integer i = 0;
	
	public Object process(Object item) throws Exception {
		i = i + 1;
		util.test();
//		System.out.println(i);
        return item;
	}

}
